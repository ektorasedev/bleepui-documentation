$(document).ready(function() {

    var hash = window.location.hash.substr(1);
    var href = $('#navUl li a').each(function(){
        var href = $(this).attr('href');
        if(hash === href.substr(0,href.length-5)){
            var toLoad = hash+'.html #content';
            $('#content').load(toLoad)
        }
    });

    $('#navUl li a').click(function(){
        var toLoad = $(this).attr('href')+' #content';
        $('#content').hide('fast',loadContent);
        $('#load').remove();
        $('#wrapper').append('<span id="load">LOADING...</span>');
        $('#load').fadeIn('normal');
        function loadContent() {
            $('#content').load(toLoad,'',showNewContent())
        }
        function showNewContent() {
            $('#content').show('normal',hideLoader());
        }
        function hideLoader() {
            $('#load').fadeOut('normal');
        }
        return false;
    });
});

$(document).ready(function () {

    var hash = window.location.hash.substr(1);
    var select = $('#navSel').each(function(){
        var select = $(this).val();
        if(hash === select.substr(0,select.length-5)){
            var toLoad = hash+'.html #content';
            $('#content').load(toLoad)
        }
    });

    $('#navSel').on('change', function(){
        var toLoad = $(this).val()+' #content';
        $('#content').hide('fast',loadContent);
        $('#load').remove();
        $('#wrapper').append('<span id="load">LOADING...</span>');
        $('#load').fadeIn('normal');
        function loadContent() {
            $('#content').load(toLoad,'',showNewContent())
        }
        function showNewContent() {
            $('#content').show('normal',hideLoader());
        }
        function hideLoader() {
            $('#load').fadeOut('normal');
        }
        return false;
    });
});

function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
}