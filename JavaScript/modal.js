function modalWindow(modalButton,modalName,modalTitle,modalContent) {

    var theModal = document.createElement("div");
    var content = document.createElement("div");
    var header = document.createElement("div");
    var span = document.createElement("span");
    var title = document.createElement("h2");
    var body = document.createElement("div");
    var par = document.createElement("p");

    theModal.setAttribute("id", modalName);
    theModal.setAttribute("class","modal");
    content.setAttribute("class","modal-content");
    header.setAttribute("class", "modal-header");
    span.setAttribute("class","close");
    body.setAttribute("class","modal-body");

    span.innerHTML = "&times;";
    title.textContent = modalTitle;
    par.textContent = modalContent;

    document.getElementsByTagName("body")[0].appendChild(theModal);
    theModal.appendChild(content);
    content.appendChild(header);
    header.appendChild(span);
    header.appendChild(title);
    content.appendChild(body);
    body.appendChild(par);

    var modal = document.getElementById(modalName);
    var btn = document.getElementById(modalButton);
    var close = document.getElementsByClassName("close")[0];

    btn.onclick = function() {
        modal.style.display = "block";
    };
    close.onclick = function() {
        modal.style.display = "none";
    };
    window.onclick = function(event) {
        if (event.target === modal) {
            modal.style.display = "none";
        }
    }
}